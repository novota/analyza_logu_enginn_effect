
# define constants
WORKING_DIR = "/Users/petrnovota/python_projects/analyza_logu_enginn_effect"

# target data that will be extracted
TIME = "Time"
P_RGH = "p_rgh"
OMEGA = "omega"
K = "k"

# target data value identifiers, i.e. how do we
IDENTIFIER_PREFIX = "Solving for "
TARGET_IDENTIFIERS = {
    TIME: "Time = ",
    K: f"{IDENTIFIER_PREFIX}{K}, ",
    P_RGH: f"{IDENTIFIER_PREFIX}{P_RGH}, ",
    OMEGA: f"{IDENTIFIER_PREFIX}{OMEGA}, "
}

# define pandas dataframe structure
DATA_COLUMNS = [P_RGH, OMEGA, K]
INDEX = TIME

# define measurement identifiers
MEASUREMENT_START = "Interface Courant Number mean: "
MEASUREMENT_END = "weightedSum(Vystup)"

# VISUALISATION CONSTANTS
FONTSIZE = 30
LINEWIDTH = 4

