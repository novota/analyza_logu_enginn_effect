# How to
In here you will learn how to use classes CheckResidual and DynamicTermination

## Data
All data used as input must be in the *data/* folder and conform to the names either **log.run** or **alpha.water**

## How to run classes
Install all dependencies listed in **requirements.txt**

Clases were tested on the latest Python 3.10 and latest packages

run: **check_residual.py** or **dynamic_termination.py** script from command line, e.g. `python check_residual.py`, `python dynamic_termination.py`

## Outputs
check_residual.py outputs target values in the *data/* folder as .csv and .pkl. Also it creates
a plot that can be found inside *visualisations/* folder

## Settings
The *settings.py* file contains various settings for both classes. It should allow for easier
maintenance in case input file changes, or the program doesn not work properly for some 
unforeseen edge case etc...

## Visualisations
All visualisations are in the *visualisations.py* file

## Tests
Critical functions were tested. Test data are in the *tests/data/* folder, and the test scripts are in their respective folders.
The folder names indicate which part of the program is being tested

## Issues
No issues are known at this moment. If you encounter any troubles, please, immediately let me know  at
novotapetr@seznam.cz or you can reach me on my phone: 602 402 716 if it is urgent.

