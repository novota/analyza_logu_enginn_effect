import re
import typing

from pathlib import Path


def get_numbers_from_string(text: str) -> typing.List[str]:
    """Get all numbers from a string."""

    return re.findall(r"[-+]?(?:\d*\.?\d+)(?:\Be[-+]?\d+)?", text)


def create_dir(parent_dir: Path, new_dir_name: str):
    """Create new directory in parent directory. If it already exists, do nothing."""

    new_dir_path = parent_dir / new_dir_name
    if not new_dir_path.exists():
        new_dir_path.mkdir(parents=True, exist_ok=True)

    return new_dir_path
