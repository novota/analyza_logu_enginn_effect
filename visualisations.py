
import logging
import matplotlib.pyplot as plt
import settings
import utilities

from pathlib import Path

logging.basicConfig(level=logging.INFO)


def check_residual_visualisation(time, p_rgp, omega, k):
    """Create graph designed for CheckResidual class result."""

    fig_save_path = utilities.create_dir(Path(settings.WORKING_DIR), "visualistaions")
    # create file name
    fig_save_path = fig_save_path / f"CheckResidual"

    # plot and save result
    plt.figure(figsize=(40, 30))

    plt.plot(p_rgp, "b", omega, "r", k, "g", linewidth=settings.LINEWIDTH)
    plt.yscale("log")
    # plot style
    plt.xticks(fontsize=settings.FONTSIZE)
    plt.yticks(fontsize=settings.FONTSIZE)
    plt.grid(axis="both")
    # graph description
    plt.xlabel("Time [s]", fontsize=settings.FONTSIZE)
    plt.ylabel("Residual", fontsize=settings.FONTSIZE)
    plt.title(f"CheckResidual results", fontsize=settings.FONTSIZE)
    plt.legend([f"p_rgp", "omega", "k"], fontsize=settings.FONTSIZE, loc="upper right")
    plt.savefig(fig_save_path)

    logging.info(f"Visualisation saved to: {fig_save_path}")
