import logging
import pandas as pd
import re
import settings
import utilities

from pathlib import Path

import visualisations

logging.basicConfig(level=logging.INFO)


class CheckResidual:
    input_path = Path(f"{settings.WORKING_DIR}/data/log.run")
    measurement_started = False  # indicates whether we are in section of ongoing measurement or not
    measurement_data = {}
    df_results = None

    def __init__(self, input_path: Path = None):
        self.data = []
        if input_path is not None:
            self.input_path = input_path

    def run(self) -> None:
        """Read log.run file, parse line by line and look for target values."""

        logging.info(f"reading input data from {settings.WORKING_DIR}/data/log.run")
        with open(self.input_path, "r") as file:
            file_content = file.readlines()

        logging.info(f"Parsing input file line by line")
        [self._parse_line(line=line) for line in file_content]

        # convert list of dicts to pandas Dataframe
        self.df_results = pd.DataFrame(self.data)
        self.df_results.set_index(settings.TIME, inplace=True)

        # save result in file
        logging.info(f"saving results to: {settings.WORKING_DIR}/data/log_run.csv")
        self.df_results.to_csv(f"{settings.WORKING_DIR}/data/log_run.csv")
        self.df_results.to_pickle(f"{settings.WORKING_DIR}/data/log_run.pkl")

    def _parse_line(self, line: str) -> None:
        """Parse line."""

        # find where measurement starts
        if not self.measurement_started:
            if line.find(settings.MEASUREMENT_START) != -1:
                self.measurement_started = True
                # The measurement start can be identified by the same line where Time is located
                self._find_data(line=line)
            return

        # measurement section
        # check for measurement end
        if line.find(settings.MEASUREMENT_END) != -1:
            # one measurement has ended, save results
            self._write_data()
            # make sure that if there is any target value missing we dont use the one from previous measurement
            self.measurement_data.clear()
            self.measurement_started = False
            return

        # ongoing measurement, search for target values, dont presume any particular target values order
        self._find_data(line)

    def _find_data(self, line: str) -> None:
        """Search in current line whether there is data we are interested in. If so, save it."""

        if re.findall(fr"\b{settings.TARGET_IDENTIFIERS.get(settings.TIME)}", line):
            self._extract_time(line)
            return
        for target in settings.DATA_COLUMNS:
            if re.findall(fr"\b{settings.TARGET_IDENTIFIERS.get(target)}", line):
                value = self._extract_target(line)
                self.measurement_data[target] = value
                return

    def _extract_time(self, line: str) -> None:
        """Extract time value from string. The assumption is that there is only one number in the string."""

        numbers: list = utilities.get_numbers_from_string(line)
        # any numbers were found
        if numbers:
            # only one number is expected
            if len(numbers) == 1:
                number = float(numbers[0])
                self.measurement_data[settings.TIME] = number
                # logging.info(f"{settings.TIME}: {number} saved.")
                return
        logging.error(f"There are too many or no numbers found then expected. Cannot find correct value for {settings.TIME} from: {numbers}")
        exit(1)

    @staticmethod
    def _extract_target(line) -> float:
        """Extract target data from string. The assumption is that the second number in the string is the value we are looking for."""

        numbers: list = utilities.get_numbers_from_string(line)
        # make sure there are any numbers present
        if numbers:
            # make sure there are at least two numbers
            if len(numbers) >= 2:
                return float(numbers[1])
        logging.error(f"Not enough numbers found. Cannot find correct value for {settings.DATA_COLUMNS} from: {numbers}")
        exit(1)

    def _write_data(self) -> None:
        """Save data in a list."""

        # all values are saved together, split it into index and values and save it to dataframe
        self.data.append(self.measurement_data.copy())

    def visualize_results(self) -> None:
        """Visualize results."""

        visualisations.check_residual_visualisation(time=self.df_results.index, p_rgp=self.df_results.loc[:, settings.P_RGH],
                                                    omega=self.df_results.loc[:, settings.OMEGA], k=self.df_results.loc[:, settings.K])


if __name__ == "__main__":
    cr = CheckResidual()
    cr.run()
    cr.visualize_results()
