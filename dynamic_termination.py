
from pathlib import Path

import logging
import pandas as pd
import settings
import utilities

logging.basicConfig(level=logging.INFO)


class DynamicTermination:

    df_results = None

    def __init__(self):
        # this is where data is stored during parsing
        self.input_data = []

    def run(self) -> None:
        """Run class."""

        # open source file
        p = Path(settings.WORKING_DIR) / f"data/alpha.water"
        with open(p, "r") as f:
            file_content = f.readlines()

        # parse it line by line and look for value pairs
        [self._parse_line(line) for line in file_content]

        # convert parsed input to dataframe
        self.df_results = pd.DataFrame(self.input_data, columns=[settings.TIME, "water_fraction"], dtype="float64")
        self.df_results.set_index(settings.TIME, inplace=True)

        self._find_termination_time()

    def _parse_line(self, line: str) -> None:
        """Find and save number pair from string.
        Valid data line consists of two numbers, extract them. The first one is Time, the second one is water fraction.
        """

        numbers = utilities.get_numbers_from_string(line)
        if len(numbers) != 2:
            logging.info(f"This line: {line} : does not contain two numbers and is skipped!")
            return
        self.input_data.append(numbers)

    def _find_termination_time(self):
        """Find whether termination condition has been met or not.
        Condition 1: abs(min - max) in last 20000 lines < 0.1
        Condition 2: Water_fraction(T) < 0.5
        """

        # Condition 1
        delta = (self.df_results.rolling(20000).max() - self.df_results.rolling(20000).min()).abs()
        condition1 = delta["water_fraction"] < 0.1

        # Condition 2
        condition2 = self.df_results["water_fraction"] < 0.5

        # apply both conditions on the dataframe
        candidates = self.df_results.loc[condition1 & condition2, "water_fraction"]

        if candidates.empty:
            logging.info(f"The termination condition was not yet met!")
            return
        logging.info(f"The termination condition was met at Time: {candidates.index[0]}!")


if __name__ == "__main__":
    dt = DynamicTermination()
    dt.run()



