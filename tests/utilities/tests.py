import logging

import utilities

logging.basicConfig(level=logging.INFO)


def _evaluate_number_equality(exctracted_number, number) -> bool:
    try:
        exctracted_number = int(exctracted_number)
    except ValueError:
        exctracted_number = float(exctracted_number)
    if not exctracted_number == number:
        logging.error(f"Extracted number: {exctracted_number} != {number}")
        return False
    return True


def test_get_number_from_string():
    """Test get_number_from_string function."""

    single_digit = 5
    single_digit_negative = -7
    integer_long = 453623984
    simple_float = 123.988343
    scientific_notation_float = 23.34534e-10
    scientific_notation_float_big = 33.2342e10

    numbers = [single_digit, single_digit_negative, integer_long, simple_float, scientific_notation_float, scientific_notation_float_big]

    text1 = "Some: text= with diff öasd"
    text2 = "Another * text!"
    success = True

    for number in numbers:
        # number at the start
        extracted_number = utilities.get_numbers_from_string(f"{number} {text1} {text2}")[0]
        success = success and _evaluate_number_equality(extracted_number, number)

        # number in the middle
        extracted_number = utilities.get_numbers_from_string(f"{text1} {number} {text2}")[0]
        success = success and _evaluate_number_equality(extracted_number, number)

        # number at the end
        extracted_number = utilities.get_numbers_from_string(f"{text1} {text2} {number}")[0]
        success = success and _evaluate_number_equality(extracted_number, number)
    if success:
        logging.info(f"test_get_number_from_string: ALL TESTS PASSED!")
        return
    logging.info(f"SOME TESTS FAILED!! Check the log for more info!")


if __name__ == "__main__":
    test_get_number_from_string()
