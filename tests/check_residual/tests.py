import logging

import pandas as pd

import settings
from check_residual import CheckResidual
from pathlib import Path


def test_check_residual():
    working_dir = Path("/Users/petrnovota/python_projects/analyza_logu_enginn_effect/tests/data/")
    # test scenarios
    basic = working_dir / "basic.run"
    two_pages = working_dir / "two_pages.run"
    k_value_missing = working_dir / "k_value_missing.run"

    # expected results
    basic_res = pd.DataFrame([[3.7704275e-11, 7.6312466e-09, 8.4599796e-08]], columns=settings.DATA_COLUMNS, index=[0.300024])
    basic_res.index.name = "Time"
    two_pages_res = pd.DataFrame([[3.7704275e-11, 7.6312466e-09, 8.4599796e-08], [3.7704275e-10, 9.6312466e09, 3.4599796e-08]], columns=settings.DATA_COLUMNS, index=[0.300024, 0.300026])
    two_pages_res.index.name = "Time"
    k_value_missing_res = pd.DataFrame([[3.7704275e-11, 7.6312466e-09, 8.4599796e-08], [3.7704275e-10, 9.6312466e09]], columns=settings.DATA_COLUMNS, index=[0.300024, 0.300026])
    k_value_missing_res.index.name = "Time"
    # k_value_missing_res.iloc[1,2] = np.nan

    test_set = [(basic, basic_res), (two_pages, two_pages_res), (k_value_missing, k_value_missing_res)]
    for test, exp_res in test_set:
        cr = CheckResidual(input_path=test)
        cr.run()
        df_res = pd.read_pickle(Path(settings.WORKING_DIR) / "data/log_run.pkl")
        if (df_res.dropna().reset_index(drop=True) == exp_res.dropna().reset_index(drop=True)).all().all():
            logging.info(f"Test passed!")
        else:
            logging.error(f"Test failed!\n{df_res}\ndoes not equal\n{exp_res}")


if __name__ == "__main__":
    test_check_residual()
